package com.wavelabs.model.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.wavelabs.builder.ObjectBuilder;
import com.wavelabs.model.UserAdvertIntrested;

@RunWith(MockitoJUnitRunner.class)
public class UserAdvertIntrestedTest {

	@InjectMocks
	private UserAdvertIntrested uai;

	private UserAdvertIntrested uai1;

	@Before
	public void setUp() {
		uai1 = ObjectBuilder.getUserAdvert();
	}

	@Test
	public void testUserAdvertIntrestedId() {
		uai.setId(uai1.getId());
		Assert.assertEquals(uai1.getId(), uai.getId());
	}

	@Test
	public void testGetAdvertisement() {

		uai.setAdvert(uai1.getAdvert());
		Assert.assertEquals((uai1.getAdvert().getName()), (uai.getAdvert().getName()));
	}

	@Test
	public void testGetUser() {

		uai.setUser(uai1.getUser());
		Assert.assertEquals(uai1.getUser().getTenantId(), uai1.getUser().getTenantId());
	}

	@Test
	public void testUserAdvertConstructor() {
		uai = new UserAdvertIntrested(1, ObjectBuilder.getUser(), ObjectBuilder.getAdvertisement());
		Assert.assertEquals(1, uai.getId());
	}
}
