package com.wavelabs.model.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.wavelabs.builder.ObjectBuilder;
import com.wavelabs.model.User;

@RunWith(MockitoJUnitRunner.class)
public class UserTest {

	@InjectMocks
	private User user1;

	private User user;

	@Before
	public void setUp() {

		user = ObjectBuilder.getUser();

	}

	@Test
	public void testGetUserId() {
		user1.setId(user.getId());
		Assert.assertEquals(user.getId(), user1.getId());
	}

	@Test
	public void testGetTenantId() {
		user1.setTenantId(user.getTenantId());
		Assert.assertEquals(user.getTenantId(), user1.getTenantId());
	}

	@Test
	public void testGetUuid() {
		user1.setUuid(user.getUuid());
		Assert.assertEquals(user.getUuid(), user1.getUuid());
	}
}
