package com.wavelabs.model.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.wavelabs.builder.ObjectBuilder;
import com.wavelabs.model.TokenInfo;

@RunWith(MockitoJUnitRunner.class)
public class TokenInfoTest {

	@InjectMocks
	private TokenInfo tokenInfoMock;

	private TokenInfo tokenInfo;

	@Before
	public void setUp() {
		tokenInfo = ObjectBuilder.getTokenInfo();
	}

	@Test
	public void testGetId() {
		tokenInfoMock.setId(tokenInfo.getId());
		Assert.assertEquals(tokenInfo.getId(), tokenInfoMock.getId());
	}

	@Test
	public void testGetClientId() {
		tokenInfoMock.setClientId(tokenInfo.getClientId());
		Assert.assertEquals(tokenInfo.getClientId(), tokenInfoMock.getClientId());
	}

	@Test
	public void testGetPhone() {
		tokenInfoMock.setPhone(tokenInfo.getPhone());
		Assert.assertEquals(tokenInfo.getPhone(), tokenInfoMock.getPhone());
	}

	@Test
	public void testGetEmail() {
		tokenInfoMock.setEmail(tokenInfo.getEmail());
		Assert.assertEquals(tokenInfo.getEmail(), tokenInfoMock.getEmail());
	}

	@Test
	public void testGetModules() {

		tokenInfoMock.setModules(tokenInfo.getModules());
		Assert.assertEquals(tokenInfo.getModules().length, tokenInfoMock.getModules().length);
	}

	@Test
	public void testGetMember() {

		tokenInfoMock.setMember(tokenInfo.getMember());
		Assert.assertEquals(tokenInfo.getMember().getUuid(), tokenInfoMock.getMember().getUuid());
	}

	@Test
	public void testGetTokenType() {
		tokenInfoMock.setTokenType(tokenInfo.getTokenType());
		Assert.assertEquals(tokenInfo.getTokenType(), tokenInfoMock.getTokenType());
	}

	@Test
	public void testGetToken() {
		tokenInfoMock.setToken(tokenInfo.getToken());
		Assert.assertEquals(tokenInfo.getToken(), tokenInfoMock.getToken());
	}

	@Test
	public void testGetExpierd() {
		tokenInfoMock.setExpired(tokenInfo.isExpired());
		Assert.assertEquals(tokenInfo.isExpired(), tokenInfoMock.isExpired());
	}

	@Test
	public void testGetTenantId() {
		tokenInfoMock.setTenantId(tokenInfo.getTenantId());
		Assert.assertEquals(tokenInfo.getTenantId(), tokenInfoMock.getTenantId());
	}

	@Test
	public void testGetExpiration() {
		tokenInfoMock.setExpiration(tokenInfo.getExpiration());
		Assert.assertEquals(tokenInfo.getExpiration(), tokenInfoMock.getExpiration());
	}

	@Test
	public void testGetAuthorities() {

		tokenInfoMock.setAuthorities(tokenInfo.getAuthorities());
		Assert.assertEquals(tokenInfo.getAuthorities().length, tokenInfoMock.getAuthorities().length);
	}

	@Test
	public void testGetUserName() {

		tokenInfoMock.setUsername(tokenInfo.getUsername());
		Assert.assertEquals(tokenInfo.getUsername(), tokenInfoMock.getUsername());
	}
}
