package com.wavelabs.model.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.wavelabs.builder.ObjectBuilder;
import com.wavelabs.model.Authorities;

@RunWith(MockitoJUnitRunner.class)
public class AuthoritiesTest {

	@InjectMocks
	private Authorities mockAuthorities;

	private Authorities authorities;

	@Before
	public void setUp() {

		authorities = ObjectBuilder.getAuthorities();

	}

	@Test
	public void testGetAuthoritiesId() {
		mockAuthorities.setId(authorities.getId());
		Assert.assertEquals(authorities.getId(), mockAuthorities.getId());
	}

	@Test
	public void testGetUAuthorityName() {
		mockAuthorities.setuAuthorityName(authorities.getuAuthorityName());
		Assert.assertEquals(authorities.getuAuthorityName(), mockAuthorities.getuAuthorityName());
	}

	@Test
	public void testDisplayName() {

		mockAuthorities.setDisplayName(authorities.getDisplayName());
		Assert.assertEquals(authorities.getDisplayName(), mockAuthorities.getDisplayName());
	}

	@Test
	public void testDescription() {
		mockAuthorities.setDescription(authorities.getDescription());
		Assert.assertEquals(authorities.getDescription(), mockAuthorities.getDescription());
	}
}
