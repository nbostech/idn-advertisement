/*package com.wavelabs.controller.test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.ResponseEntity;

import com.wavelabs.builder.ObjectBuilder;
import com.wavelabs.model.Advertisement;
import com.wavelabs.model.TokenInfo;
import com.wavelabs.resource.AdvertisementResource;
import com.wavelabs.service.AdvertisementService;
import com.wavelabs.solr.service.SolrSearchService;
import com.wavelabs.utility.UserTypeChecker;

@RunWith(PowerMockRunner.class)
@PrepareForTest(UserTypeChecker.class)
public class AdvertisementControllerTest {

	@Mock
	private SolrSearchService solrService;

	@Mock
	private AdvertisementService addService;

	@InjectMocks
	private AdvertisementResource resource;

	@Test
	public void testGetAdvertisement() throws Exception {

		PowerMockito.mockStatic(UserTypeChecker.class);
		Advertisement add = ObjectBuilder.getAdvertisement();
		TokenInfo tokenIno = ObjectBuilder.getTokenInfo();
		when(addService.getAdvertisement(anyInt())).thenReturn(add);
		when(UserTypeChecker.isGuestUser(any())).thenReturn(false);
		ResponseEntity entity = resource.getAdvertisement(2, tokenIno);
		Assert.assertEquals(200, entity.getStatusCodeValue());
	}

	@Test
	public void testGetAdvertisement2() throws Exception {

		PowerMockito.mockStatic(UserTypeChecker.class);
		Advertisement add = ObjectBuilder.getAdvertisement();
		TokenInfo tokenIno = ObjectBuilder.getTokenInfo();
		when(addService.getAdvertisement(anyInt())).thenReturn(add);
		when(UserTypeChecker.isGuestUser(any())).thenReturn(true);
		ResponseEntity entity = resource.getAdvertisement(2, tokenIno);
		Assert.assertEquals(403, entity.getStatusCodeValue());
	}

	@Test
	public void testGetAdvertisement3() throws Exception {

		PowerMockito.mockStatic(UserTypeChecker.class);
		Advertisement add = ObjectBuilder.getAdvertisement();
		TokenInfo tokenIno = ObjectBuilder.getTokenInfo();
		when(addService.getAdvertisement(anyInt())).thenReturn(null);
		when(UserTypeChecker.isGuestUser(any())).thenReturn(false);
		ResponseEntity entity = resource.getAdvertisement(2, tokenIno);
		Assert.assertEquals(404, entity.getStatusCodeValue());
	}

	@Test
	public void testGetAdvertisement4() throws Exception {

		PowerMockito.mockStatic(UserTypeChecker.class);
		Advertisement add = ObjectBuilder.getAdvertisement();
		TokenInfo tokenIno = ObjectBuilder.getTokenInfo();
		when(addService.getAdvertisement(anyInt())).thenReturn(add);
		when(UserTypeChecker.isGuestUser(any())).thenReturn(false);
		ResponseEntity entity = resource.getAdvertisement(2, tokenIno);
		Assert.assertEquals(200, entity.getStatusCodeValue());
	}

	@Test
	public void testPersistAdvertisement1() {

		PowerMockito.mockStatic(UserTypeChecker.class);
		Advertisement add = ObjectBuilder.getAdvertisement();
		TokenInfo tokenInfo = ObjectBuilder.getTokenInfo();
		when(addService.persistAdvertisement(any())).thenReturn(true);
		when(UserTypeChecker.isGuestUser(any())).thenReturn(false);
		ResponseEntity entity = resource.saveAdvertisement(add, tokenInfo);
		Assert.assertEquals(201, entity.getStatusCodeValue());
	}

	@Test
	public void testPersistAdvertisement2() {

		PowerMockito.mockStatic(UserTypeChecker.class);
		Advertisement add = ObjectBuilder.getAdvertisement();
		TokenInfo tokenInfo = ObjectBuilder.getTokenInfo();
		when(addService.persistAdvertisement(any())).thenReturn(false);
		when(UserTypeChecker.isGuestUser(any())).thenReturn(false);
		ResponseEntity entity = resource.saveAdvertisement(add, tokenInfo);
		Assert.assertEquals(500, entity.getStatusCodeValue());
	}

	@Test
	public void testPersistAdvertisement3() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		Advertisement add = ObjectBuilder.getAdvertisement();
		TokenInfo tokenInfo = ObjectBuilder.getTokenInfo();
		when(addService.persistAdvertisement(any())).thenReturn(false);
		when(UserTypeChecker.isGuestUser(any())).thenReturn(true);
		ResponseEntity entity = resource.saveAdvertisement(add, tokenInfo);
		Assert.assertEquals(403, entity.getStatusCodeValue());
	}
	@Test
	public void testUpdateAdvertisement1() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		Advertisement add = ObjectBuilder.getAdvertisement();
		TokenInfo tokenInfo = ObjectBuilder.getTokenInfo();
		when(addService.updateAdvertisement(any())).thenReturn(add);
		when(UserTypeChecker.isGuestUser(any())).thenReturn(false);
		ResponseEntity entity = resource.updateAdvertisement(1,add, tokenInfo);
		Assert.assertEquals(200, entity.getStatusCodeValue());
	}
	@Test
	public void testUpdateAdvertisement2() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		Advertisement add = ObjectBuilder.getAdvertisement();
		TokenInfo tokenInfo = ObjectBuilder.getTokenInfo();
		when(addService.updateAdvertisement(any())).thenReturn(null);
		when(UserTypeChecker.isGuestUser(any())).thenReturn(false);
		ResponseEntity entity = resource.updateAdvertisement(1,add, tokenInfo);
		Assert.assertEquals(500, entity.getStatusCodeValue());
	}
	@Test
	public void testUpdateAdvertisement3() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		Advertisement add = ObjectBuilder.getAdvertisement();
		TokenInfo tokenInfo = ObjectBuilder.getTokenInfo();
		when(addService.updateAdvertisement(any())).thenReturn(add);
		when(UserTypeChecker.isGuestUser(any())).thenReturn(true);
		ResponseEntity entity = resource.updateAdvertisement(1,add, tokenInfo);
		Assert.assertEquals(403, entity.getStatusCodeValue());
	}
	@Test
	public void testDeleteAdvertisement1() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		Advertisement add = ObjectBuilder.getAdvertisement();
		TokenInfo tokenInfo = ObjectBuilder.getTokenInfo();
		when(addService.deleteAdvertisement(anyInt())).thenReturn(true);
		when(UserTypeChecker.isGuestUser(any())).thenReturn(false);
		ResponseEntity entity = resource.deleteAdvertisement(1,tokenInfo);
		Assert.assertEquals(200, entity.getStatusCodeValue());
	}
	@Test
	public void testDeleteAdvertisement2() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		Advertisement add = ObjectBuilder.getAdvertisement();
		TokenInfo tokenInfo = ObjectBuilder.getTokenInfo();
		when(addService.deleteAdvertisement(anyInt())).thenReturn(false);
		when(UserTypeChecker.isGuestUser(any())).thenReturn(false);
		ResponseEntity entity = resource.deleteAdvertisement(1,tokenInfo);
		Assert.assertEquals(500, entity.getStatusCodeValue());
	}
	@Test
	public void testDeleteAdvertisement3() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		Advertisement add = ObjectBuilder.getAdvertisement();
		TokenInfo tokenInfo = ObjectBuilder.getTokenInfo();
		when(addService.deleteAdvertisement(anyInt())).thenReturn(false);
		when(UserTypeChecker.isGuestUser(any())).thenReturn(true);
		ResponseEntity entity = resource.deleteAdvertisement(1,tokenInfo);
		Assert.assertEquals(403, entity.getStatusCodeValue());
	}
	
	
	@Test
	public void testSearchAdd() {
		
		Advertisement[] a = {ObjectBuilder.getAdvertisement()};
		TokenInfo tokenInfo = ObjectBuilder.getTokenInfo();
		when(solrService.getAdverts(anyString())).thenReturn(a);
		
	}
}
*/