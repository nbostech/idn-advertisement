package com.wavelabs.builder;

import java.util.Calendar;
import com.wavelabs.model.Advertisement;
import com.wavelabs.model.Authorities;
import com.wavelabs.model.Comment;
import com.wavelabs.model.Member;
import com.wavelabs.model.Message;
import com.wavelabs.model.Modules;
import com.wavelabs.model.TokenInfo;
import com.wavelabs.model.User;
import com.wavelabs.model.UserAdvertIntrested;
import com.wavelabs.model.enums.AdvertisementType;

public class ObjectBuilder {

	public static User getUser() {

		User user = new User();
		user.setId(1);
		user.setTenantId("wavelabs");
		user.setUuid("nbos");
		return user;
	}

	public static Advertisement getAdvertisement() {
		Advertisement add = new Advertisement();
		add.setId(1);
		add.setName("Phone add");
		add.setDescription("Something");
		add.setLocation("Hyderabad");
		add.setType(AdvertisementType.BUSINESS);
		add.setUser(getUser());
		return add;
	}

	public static Modules getModule() {

		Modules module = new Modules();
		module.setName("AdvertisementGopi");
		module.setUuid("123456");
		return module;
	}

	public static Member getMember() {

		Member member = new Member();
		member.setUuid("123456");
		return member;

	}

	public static Authorities getAuthorities() {

		Authorities auth = new Authorities();
		auth.setId(1);
		auth.setuAuthorityName("admin");
		auth.setDisplayName("gopi krishna");
		auth.setDescription("Something");
		return auth;
	}

	public static Comment getComment() {
		Comment comment = new Comment();
		comment.setId(1);
		comment.setText("This is test comment");
		comment.setTimeStamp(Calendar.getInstance());
		comment.setAdvertisement(getAdvertisement());
		comment.setUser(getUser());
		return comment;
	}

	public static UserAdvertIntrested getUserAdvert() {
		UserAdvertIntrested uai = new UserAdvertIntrested();
		uai.setAdvert(getAdvertisement());
		uai.setUser(getUser());
		return uai;
	}

	public static Message getMessage() {
		Message message = new Message();
		message.setId(200);
		message.setText("This is something");
		return message;
	}

	public static TokenInfo getTokenInfo() {
		TokenInfo tokenInfo = new TokenInfo();
		tokenInfo.setId(1l);
		tokenInfo.setAuthorities(new Authorities[] { getAuthorities() });
		tokenInfo.setClientId("abc");
		tokenInfo.setEmail("gopi@gmail.com");
		tokenInfo.setExpired(false);
		tokenInfo.setMember(getMember());
		tokenInfo.setTenantId("olx");
		tokenInfo.setExpiration(Calendar.getInstance());
		tokenInfo.setModules(new Modules[] { getModule() });
		tokenInfo.setTokenType("Bearer");
		tokenInfo.setToken("token");
		tokenInfo.setPhone("9032118864");
		tokenInfo.setUsername("gopi279");
		return tokenInfo;
	}
}
