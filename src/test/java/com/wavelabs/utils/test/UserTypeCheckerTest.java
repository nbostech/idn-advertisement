package com.wavelabs.utils.test;

import static org.junit.Assert.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.wavelabs.builder.ObjectBuilder;
import com.wavelabs.model.Authorities;
import com.wavelabs.model.TokenInfo;
import com.wavelabs.utility.UserTypeChecker;

@RunWith(MockitoJUnitRunner.class)
public class UserTypeCheckerTest {

	@Test
	public void testIsAdminfalse() {

		TokenInfo tokenInfo = ObjectBuilder.getTokenInfo();
		boolean flag = UserTypeChecker.isAdmin(tokenInfo);
		Assert.assertEquals(false, flag);
	}

	@Test
	public void testIsAdmintrue() {
		TokenInfo tokenInfo = ObjectBuilder.getTokenInfo();
		Authorities[] auth = tokenInfo.getAuthorities();
		for (Authorities a : auth) {
			a.setuAuthorityName("authority:core.module.admin");
		}
		boolean flag = UserTypeChecker.isAdmin(tokenInfo);
		Assert.assertEquals(true, flag);
	}

	@Test
	public void testIsGuestUser1() {
		TokenInfo tokenInfo = ObjectBuilder.getTokenInfo();
		boolean flag = UserTypeChecker.isGuestUser(tokenInfo);
		Assert.assertEquals(false, flag);
	}
	@Test
	public void testIsGuestUser2() {
		TokenInfo tokenInfo = ObjectBuilder.getTokenInfo();
		tokenInfo.setUsername(null);
		boolean flag = UserTypeChecker.isGuestUser(tokenInfo);
		Assert.assertEquals(true, flag);
	}
	@Test
	public void testConstructorIsPrivate() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
	  Constructor<UserTypeChecker> constructor = UserTypeChecker.class.getDeclaredConstructor();
	  assertTrue(Modifier.isPrivate(constructor.getModifiers()));
	  constructor.setAccessible(true);
	  constructor.newInstance();
	}
}
