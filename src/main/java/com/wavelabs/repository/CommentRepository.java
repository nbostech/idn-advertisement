package com.wavelabs.repository;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wavelabs.model.Advertisement;
import com.wavelabs.model.Comment;
import com.wavelabs.repo.AdvertisementRepo;
import com.wavelabs.repo.CommentRepo;

@Component
public class CommentRepository {

	@Autowired
	private CommentRepo commentRepo;

	@Autowired
	private AdvertisementRepo advertisementRepo;

	private static final Logger LOGGER = Logger.getLogger(CommentRepository.class);

	public Boolean persistComment(Comment comment) {

		try {
			commentRepo.save(comment);
			return true;
		} catch (Exception e) {
			LOGGER.error(e);
			return false;
		}
	}

	public Comment[] getAdvertisementComments(int id) {

		try {
			Advertisement advertisement = advertisementRepo.findById(id);
			Comment[] arrayOfComments;
			if(advertisement!=null) {
				 arrayOfComments = commentRepo.findAllByAdvertisement(advertisement);	
			}
			else {
				 throw new Exception("No advertisement found");
			}
			return arrayOfComments;
		} catch (Exception e) {
			LOGGER.error(e);
			return new Comment[0];
		}
	}

	public Comment updateComment(Comment comment) {

		try {
			commentRepo.save(comment);
			return comment;
		} catch (Exception e) {
			LOGGER.error(e);
			return null;
		}
	}

	public Boolean deleteComment(int id) {

		try {
			commentRepo.delete(id);
			return true;
		} catch (Exception e) {
			LOGGER.error(e);
			return false;
		}
	}
}
