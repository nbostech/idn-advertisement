package com.wavelabs;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.wavelabs.model.Message;
import com.wavelabs.model.Modules;
import com.wavelabs.model.TokenInfo;
import com.wavelabs.model.User;
import com.wavelabs.persist.Persister;
import com.wavelabs.record.existence.ObjectChecker;
import com.wavelabs.utils.Constants;

/**
 * This the Filter class to filter all the User requests for Auth tokens. This
 * would validate if the requested URL required user login. If it requires user
 * login, this class would authenticate the request with NBOS.in
 * 
 * @author gopikrishnag
 *
 */
@Component
@Order(1)
public class NbosFilter implements Filter {
	@Autowired
	private Environment env;

	@Autowired
	private ObjectChecker objectChecker;

	@Autowired
	private Persister persister;

	private static final String moduleToken = "MOD:11296761-d4d8-4975-b27f-41090a607702";

	/*
	 * @Autowired private AuthenticationTokenCache cache;
	 */

	private static Logger logger = LoggerFactory.getLogger(NbosFilter.class);

	private AuthenticationTokenCache authTokenCache = AuthenticationTokenCache.getInstance();

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig config) throws ServletException {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	public void destroy() {
	}

	/**
	 * Method to filter all the user requests and authenticate them
	 * 
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest,
	 *      javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		final HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		final HttpServletResponse httpServletResponse = (HttpServletResponse) response;
		StringBuffer wurl = httpServletRequest.getRequestURL();
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		httpResponse.addHeader("Access-Control-Allow-Origin", "*");
		if (needLogin(httpServletRequest)) {
			String methodName = httpServletRequest.getMethod();
			if ("OPTIONS".equalsIgnoreCase(methodName)) {
				httpResponse.addHeader("Access-Control-Allow-Headers",
						"origin, Authorization, accept, content-type, x-requested-with");
				httpResponse.addHeader("Access-Control-Allow-Methods", "GET, HEAD, POST, PUT, DELETE, TRACE, OPTIONS");
				httpResponse.addHeader("Access-Control-Mx-Age", "3600");
				httpResponse.addHeader("server", "nginx/1.10.3");
				httpResponse.addHeader("connection", "keep-alive");
				httpResponse.setStatus(200);
				httpResponse.getWriter().flush();
			} else {
				String accessToken = httpServletRequest.getHeader("Authorization");
				if (wurl.toString().contains("api/adv/v1/swagger-ui.html")
						|| wurl.toString().contains("api/adv/v1/swagger-resources")
						|| wurl.toString().contains("api/adv/v1/v2/api-docs")) {
					accessToken = "08f0d231-23c4-49c4-b52a-ee6c516591d3";
				} else {
					accessToken = accessToken.replace("Bearer ", "");
				}
				String url = env.getProperty(Constants.AUTH_URL);
				String moduleKey = env.getProperty(Constants.MODULE_KEY_LABEL);
				String authorization = Constants.BEARER + env.getProperty(Constants.VERIFY_TOKEN);
				logger.info("validating the user Authentication Token");
				if (authTokenCache.validateAuthToken(request, accessToken, authorization, moduleKey, url)) {
					TokenInfo tokenInfo = (TokenInfo) request.getAttribute("tokenInfo");
					boolean flag = isModuleSubscribed(tokenInfo);
					flag = true;
					if (flag) {
						if (tokenInfo.getUsername() != null) {
							createUserIfNotPresent(tokenInfo);
							httpServletRequest.setAttribute("tokenInfo", tokenInfo);
						}
						chain.doFilter(httpServletRequest, httpServletResponse);
					} else {
						Message message = new Message();
						message.setId(403);
						message.setText("You are not subscribed");
						sendErrorResponse(response, message);
					}
				}
			}
		} else {
			chain.doFilter(request, response);
		}
	}

	public void createUserIfNotPresent(TokenInfo tokenInfo) {

		boolean userExist = objectChecker.isUserExist(tokenInfo.getMember().getUuid(), tokenInfo.getTenantId());
		if (userExist) {
			logger.debug("User exist in table");
		} else {
			User user = new User();
			user.setUuid(tokenInfo.getMember().getUuid());
			user.setTenantId(tokenInfo.getTenantId());
			persister.persistUser(user);
		}
	}

	private void sendErrorResponse(ServletResponse response, Message message) throws IOException {
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		httpResponse.sendError(message.getId(), objectToJsonConverter(message));
	}

	public Boolean isModuleSubscribed(TokenInfo tokenInfo) {
		boolean flag = false;
		Modules[] module = tokenInfo.getModules();
		for (Modules m : module) {
			if (m.getUuid().equals(moduleToken)) {
				flag = true;
				break;
			}
		}
		return flag;
	}

	/**
	 * check if the user request needs login, based on the URL
	 * 
	 * @param request
	 * @return
	 */
	private Boolean needLogin(final HttpServletRequest httpServletRequest) {
		Boolean needed = Boolean.TRUE;
		String requestURI = httpServletRequest.getRequestURI();
		if (requestURI.indexOf("/visitor/") > 0) {
			needed = Boolean.FALSE;
		}
		return needed;
	}

	/**
	 * Method to proceed to the next element in the chain
	 * 
	 * @param request
	 * @param response
	 */

	public String objectToJsonConverter(Message response) {
		Gson gson = new Gson();
		return gson.toJson(response);
	}
}
