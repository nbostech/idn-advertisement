package com.wavelabs.solr.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.common.SolrInputDocument;
import org.hibernate.SessionFactory;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wavelabs.model.Advertisement;
import com.wavelabs.repo.AdvertisementRepo;

@Component
public class SolrIndexService implements Job {

	private final static Logger LOGGER = Logger.getLogger(SolrIndexService.class);
	private static SolrClient client;
	private static SessionFactory factory;

	@Autowired
	public AdvertisementRepo addsRepo;

	@SuppressWarnings("deprecation")
	private static void createSolrInstance() {
		client = new HttpSolrClient("http://localhost:8983/solr/adds");
		LOGGER.info("client connection opend");
	}

	public boolean doIndex() {
		try {
			List<Advertisement> adds = (List<Advertisement>) addsRepo.findAll();
			 addDocumentsToSolr(adds);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean addDocumentsToSolr(List<Advertisement> listOfAdvertisements) {
		int i = 0;
		createSolrInstance();
		LOGGER.debug("adding documents into solr is started");
		try {
			for (Advertisement advet : listOfAdvertisements) {
				SolrInputDocument docuemnt = new SolrInputDocument();
				docuemnt.addField("id", advet.getId());
				docuemnt.addField("name", advet.getName());
				docuemnt.addField("type",
						advet.getType().toString().replace("com.wavelabs.modal.enums.AdvertisementType:", ""));
				docuemnt.addField("description", advet.getDescription());
				docuemnt.addField("location", advet.getLocation());
				docuemnt.addField("user_id", advet.getUser().getId());
				client.add(docuemnt);
				LOGGER.info((++i) + "Document added, not commited");
			}
			client.commit();
			LOGGER.info("All documents commited");
			client.close();
			LOGGER.info("Indexing completed and connection closed");
			factory.close();
			return true;
		} catch (Exception e) {
			LOGGER.error(e);
			return false;
		}
	}

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		doIndex();
	}
}
