package com.wavelabs.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.model.Advertisement;
import com.wavelabs.model.TokenInfo;
import com.wavelabs.repo.AdvertisementRepo;
import com.wavelabs.service.AdvertisementService;
import com.wavelabs.solr.service.SolrSearchService;
import com.wavelabs.utility.UserTypeChecker;

import io.nbos.capi.api.v0.models.ForbiddenResponse;
import io.nbos.capi.api.v0.models.RestMessage;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Component
public class AdvertisementResource {

	private static String forbiddenMessage = "Please Login";

	@Autowired
	private AdvertisementService advertisementService;

	@Autowired
	private SolrSearchService solrService;

	@Autowired
	private AdvertisementRepo repo;

	public AdvertisementResource(AdvertisementService service, SolrSearchService solrService) {

		advertisementService = service;
		this.solrService = solrService;
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping("/advertisement/{id}")
	@ApiResponses({ @ApiResponse(code = 200, message = "Advertisement successfully returned"),
			@ApiResponse(code = 404, message = "Advertisement not found") })
	public ResponseEntity getAdvertisement(@ApiParam(name = "id", required = true) @PathVariable("id") int id,
			@RequestAttribute("tokenInfo") TokenInfo tokenInfo) {
		boolean flag = UserTypeChecker.isGuestUser(tokenInfo);
		flag=false;
		if (!flag) {
			Advertisement add = advertisementService.getAdvertisement(id);
			if (add == null) {
				RestMessage restMessage = new RestMessage();
				restMessage.message = "Advertisement not found in green";
				restMessage.messageCode = "404";
				return ResponseEntity.status(404).body(restMessage);
			}
			return ResponseEntity.status(200).body(add);
		} else {
			RestMessage restMessage = new RestMessage();
			restMessage.messageCode = "403";
			restMessage.message = forbiddenMessage;
			return ResponseEntity.status(403).body(restMessage);
		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/advertisement", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity saveAdvertisement(@RequestBody Advertisement add,
			@RequestAttribute("tokenInfo") TokenInfo tokenInfo) {

		boolean guestUserFlag = UserTypeChecker.isGuestUser(tokenInfo);
		guestUserFlag=false;
		if (!guestUserFlag) {
			boolean flag = advertisementService.persistAdvertisement(add);
			RestMessage restMessage = new RestMessage();
			if (flag) {
				restMessage.message = "Advertisement saved successfully";
				restMessage.messageCode = "201";
				return ResponseEntity.status(HttpStatus.CREATED).body(restMessage);
			} else {
				restMessage.message = "Advertisement saving failed";
				restMessage.messageCode = "500";
				return ResponseEntity.status(500).body(restMessage);
			}
		} else {
			ForbiddenResponse restMessage = new ForbiddenResponse();
			restMessage.messageCode = "403";
			restMessage.message = forbiddenMessage;
			return ResponseEntity.status(403).body(restMessage);
		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/advertisement/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity updateAdvertisement(@PathVariable("id") Integer id, @RequestBody Advertisement add,
			@RequestAttribute("tokenInfo") TokenInfo tokenInfo) {
		boolean flag = UserTypeChecker.isGuestUser(tokenInfo);
		flag = false;
		if (!flag) {
			add.setId(id);
			Advertisement newAdd = advertisementService.updateAdvertisement(add);
			if (newAdd != null) {
				return ResponseEntity.status(200).body(newAdd);

			} else {
				RestMessage restMessage = new RestMessage();
				restMessage.message = "Updation operation failed on server";
				restMessage.messageCode = "500";
				return ResponseEntity.status(500).body(restMessage);
			}
		} else {
			ForbiddenResponse restMessage = new ForbiddenResponse();
			restMessage.messageCode = "403";
			restMessage.message = forbiddenMessage;
			return ResponseEntity.status(403).body(restMessage);
		}
	}

	@RequestMapping(value = "/advertisement/all/{id}")
	public ResponseEntity getAllAdvertisements(@PathVariable("id") int id) {
		List<Advertisement> list = advertisementService.getAllAdvertisementOfUser(id);
		return ResponseEntity.status(200).body(list);
	}

	@RequestMapping(value = "/advertisement/all")
	public ResponseEntity getAllAdvertisements() {

		return ResponseEntity.status(200).body(repo.findAll());

	}

	@RequestMapping(value = "/advertisement/all", method = RequestMethod.OPTIONS)
	public ResponseEntity getAllAdvertisements2() {
		return ResponseEntity.status(200).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Headers", "origin, authorization, accept, content-type, x-requested-with")
				.header("server", "nginx/1.10.3").header("access-control-max-age", "3600").header("content-length", "0")
				.header("access-control-allow-headers", "origin, authorization, accept, content-type, x-requested-with")
				.body(null);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/advertisement/{id}", method = RequestMethod.DELETE)
	public ResponseEntity deleteAdvertisement(@PathVariable("id") int id,
			@RequestAttribute("tokenInfo") TokenInfo tokenInfo) {
		boolean guestUserFlag = UserTypeChecker.isGuestUser(tokenInfo);
		guestUserFlag= false;
		if (!guestUserFlag) {
			boolean flag = advertisementService.deleteAdvertisement(id);
			RestMessage restMessage = new RestMessage();
			if (flag) {
				restMessage.message = "Advertisement deleted successfully";
				restMessage.messageCode = "200";
				return ResponseEntity.status(200).body(restMessage);
			} else {
				restMessage.messageCode = "500";
				restMessage.message = "Failed delete the advertisement";
				return ResponseEntity.status(500).body(restMessage);
			}
		} else {
			ForbiddenResponse restMessage = new ForbiddenResponse();
			restMessage.messageCode = "403";
			restMessage.message = forbiddenMessage;
			return ResponseEntity.status(403).body(restMessage);
		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping("/advertisement/search/{search}")
	public ResponseEntity searchAdd(@PathVariable("search") String search,
			@RequestAttribute("tokenInfo") TokenInfo tokenInfo) {
		Advertisement adds[] = solrService.getAdverts(search);
		if (adds.length != 0) {
			return ResponseEntity.status(200).body(adds);
		} else {
			RestMessage restMessage = new RestMessage();
			restMessage.message = "Advertisements not found";
			restMessage.messageCode = "404";
			return ResponseEntity.status(404).body(restMessage);
		}
	}

}
