package com.wavelabs.resource;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.model.Message;
import com.wavelabs.model.TokenInfo;
import com.wavelabs.solr.service.SolrIndexService;
import com.wavelabs.utility.UserTypeChecker;

import io.nbos.capi.api.v0.models.RestMessage;

//@RestController
public class SolrJob {

	@Autowired
	private SolrIndexService index;

	//@RequestMapping("dataflush/on/{time}")
	public ResponseEntity startSolrJob(@PathVariable("time") Integer time,
			@RequestAttribute("tokenInfo") TokenInfo tokenInfo) {
		boolean isAdmin = UserTypeChecker.isAdmin(tokenInfo);
		if (isAdmin) {
			boolean flag = true;
			index.doIndex();
			Message message = new Message();
			if (flag) {
				message.setId(200);
				message.setText("Solr auto index completed Successfully");
				return ResponseEntity.status(200).body(message);
			} else {
				message.setId(500);
				message.setText("Problem occured");
				return ResponseEntity.status(500).body(message);
			}
		} else {
			RestMessage restMessage = new RestMessage();
			restMessage.messageCode="403";
			restMessage.message="You are not admin";
			return ResponseEntity.status(403).body(restMessage);
		}
	}
}
