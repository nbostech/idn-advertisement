package com.wavelabs.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.model.Message;
import com.wavelabs.model.TokenInfo;
import com.wavelabs.model.UserAdvertIntrested;
import com.wavelabs.service.UserAdvertIntrestService;
import com.wavelabs.utility.UserTypeChecker;

import io.nbos.capi.api.v0.models.ForbiddenResponse;
import io.nbos.capi.api.v0.models.InternalErrorResponse;
import io.nbos.capi.api.v0.models.NotFoundResponse;
import io.nbos.capi.api.v0.models.RestMessage;
import io.nbos.capi.api.v0.models.SuccessResponse;

@RestController
public class UserAdvertIntrestedResource {

	private static String forbiddenResponse = "Please login to the application";
	
	@Autowired
	private UserAdvertIntrestService service;

	public UserAdvertIntrestedResource(UserAdvertIntrestService service) {
		this.service = service;
	}

	@RequestMapping("/user/interest/{id}")
	public ResponseEntity getUserAdvertIntrest(@PathVariable("id") int id,
			@RequestAttribute("tokenInfo") TokenInfo tokenInfo) {
		boolean flag = UserTypeChecker.isGuestUser(tokenInfo);
		flag=false;
		if (!flag) {
			UserAdvertIntrested uai = service.getUserAdvert(id);
			if (uai != null) {
				return ResponseEntity.status(200).body(uai);
			} else {
				NotFoundResponse notFoundResponse = new NotFoundResponse();
				notFoundResponse.message = "No user interest found";
				notFoundResponse.messageCode = "404";
				return ResponseEntity.status(404).body(notFoundResponse);
			}
		} else {
			RestMessage restMessage = new RestMessage();
			restMessage.messageCode = "403";
			restMessage.message = forbiddenResponse;
			return ResponseEntity.status(403).body(restMessage);
		}
	}

	@RequestMapping(value = "/user/interest/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity postUserAdvertIntrest(@RequestBody UserAdvertIntrested uai,
			@RequestAttribute("tokenInfo") TokenInfo tokenInfo) {
		boolean userFlag = UserTypeChecker.isGuestUser(tokenInfo);
		userFlag=false;
		if (!userFlag) {
			boolean flag = service.persistUserAdvert(uai);
			if (flag) {
				SuccessResponse success = new SuccessResponse();
				success.message="User intrest saved successfully";
				success.messageCode="201";
				return ResponseEntity.status(201).body(success);
			} else {
				InternalErrorResponse errorResponse = new InternalErrorResponse();
				errorResponse.message="posting of user intrest is failed";
				errorResponse.messageCode="500";
				return ResponseEntity.status(500).body(errorResponse);
			}
		} else {
			RestMessage restMessage = new RestMessage();
			restMessage.messageCode = "403";
			restMessage.message = forbiddenResponse;
			return ResponseEntity.status(403).body(restMessage);
		}
	}

	@RequestMapping(value = "/user/interest/{id}", method = RequestMethod.GET)
	public ResponseEntity getUserIntrests(@PathVariable("id") int id,
			@RequestAttribute("tokenInfo") TokenInfo tokenInfo) {
		boolean flag = UserTypeChecker.isGuestUser(tokenInfo);
		flag=false;
		if (!flag) {
			List<UserAdvertIntrested> intrests = service.getAllUserAdvertIntrests(id);
			Message message = new Message();
			if (!intrests.isEmpty()) {
				NotFoundResponse notFoundResponse = new NotFoundResponse();
				notFoundResponse.message = "No User interests found";
				notFoundResponse.messageCode = "404";
				return ResponseEntity.status(200).body(message);
			} else {
				return ResponseEntity.status(500).body(intrests);
			}
		} else {
			ForbiddenResponse restMessage = new ForbiddenResponse();
			restMessage.messageCode = "403";
			restMessage.message = forbiddenResponse;
			return ResponseEntity.status(403).body(restMessage);
		}
	}
}
