package com.wavelabs.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.model.Comment;
import com.wavelabs.model.TokenInfo;
import com.wavelabs.service.CommentService;
import com.wavelabs.utility.UserTypeChecker;

import io.nbos.capi.api.v0.models.ForbiddenResponse;
import io.nbos.capi.api.v0.models.InternalErrorResponse;
import io.nbos.capi.api.v0.models.NotFoundResponse;
import io.nbos.capi.api.v0.models.RestMessage;
import io.nbos.capi.api.v0.models.SuccessResponse;

@RestController
public class CommentResource {

	@Autowired
	private CommentService commentService;

	private static String forbiddenMessage="Please Login";
	
	private CommentResource(CommentService commentService) {
		this.commentService = commentService;
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping("/advertisement/comment/{id}")
	public ResponseEntity getComment(@PathVariable("id") int id, @RequestAttribute("tokenInfo") TokenInfo tokenInfo) {
		boolean flag = UserTypeChecker.isGuestUser(tokenInfo);
		flag = false;
		if (!flag) {
			Comment[] comment = commentService.getListOfCommentsOfAdvertisement(id);
			if (comment != null) {
				return ResponseEntity.status(200).body(comment);
			} else {
				NotFoundResponse restMessage = new NotFoundResponse();
				restMessage.message="Comments not found";
				restMessage.messageCode="404";
				return ResponseEntity.status(404).body(restMessage);
			}
		} else {
			ForbiddenResponse restMessage = new ForbiddenResponse();
			restMessage.messageCode="403";
			restMessage.message=forbiddenMessage;
			return ResponseEntity.status(403).body(restMessage);
		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/advertisement/comment", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity saveComment(@RequestBody Comment comment,
			@RequestAttribute("tokenInfo") TokenInfo tokenInfo) {
		boolean guestFlag = UserTypeChecker.isGuestUser(tokenInfo);
		guestFlag=false;
		if (!guestFlag) {
			boolean flag = commentService.persistComment(comment);
			if (flag) {
				SuccessResponse restMessage = new SuccessResponse();
				restMessage.message="Comment created successfully";
				restMessage.messageCode="201";
				return ResponseEntity.status(201).body(restMessage);
			} else {
				InternalErrorResponse errorResponse = new InternalErrorResponse();
				errorResponse.message="Saving operation failed";
				errorResponse.messageCode="500";
				return ResponseEntity.status(500).body(errorResponse);
			}
		} else {
			ForbiddenResponse restMessage = new ForbiddenResponse();
			restMessage.messageCode="403";
			restMessage.message=forbiddenMessage;
			return ResponseEntity.status(403).body(restMessage);
		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/advertisement/comment/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity updateComment(@PathVariable("id") int id, @RequestBody Comment comment,
			@RequestAttribute("tokenInfo") TokenInfo tokenInfo) {
		boolean flag = UserTypeChecker.isGuestUser(tokenInfo);
		if (!flag) {
			comment.setId(id);
			Comment newComment = commentService.updateComment(comment);
			if (newComment != null) {
				return ResponseEntity.status(200).body(newComment);
			} else {
				InternalErrorResponse errorResponse = new InternalErrorResponse();
				errorResponse.message="Update operation failed";
				errorResponse.messageCode="500";
				return ResponseEntity.status(500).body(errorResponse);
			}
		} else {
			ForbiddenResponse restMessage = new ForbiddenResponse();
			restMessage.messageCode="403";
			restMessage.message=forbiddenMessage;
			return ResponseEntity.status(403).body(restMessage);
		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/advertisement/comment/{id}", method = RequestMethod.DELETE)
	public ResponseEntity deleteComment(@PathVariable("id") int id,
			@RequestAttribute("tokenInfo") TokenInfo tokenInfo) {
		boolean guestFlag = UserTypeChecker.isGuestUser(tokenInfo);
		guestFlag= false;
		if (!guestFlag) {
			boolean flag = commentService.deleteComment(id);
			if (flag) {
				SuccessResponse success = new SuccessResponse();
				success.message="Comment deleted successfullu";
				success.messageCode="200";
				return ResponseEntity.status(200).body(success);
			} else {
				InternalErrorResponse errorResponse = new InternalErrorResponse();
				errorResponse.message="Delete operation of comment failed";
				errorResponse.messageCode="500";
				return ResponseEntity.status(500).body(errorResponse);
			}
		} else {
			RestMessage restMessage = new RestMessage();
			restMessage.messageCode="403";
			restMessage.message=forbiddenMessage;
			return ResponseEntity.status(403).body(restMessage);
		}
	}
}
