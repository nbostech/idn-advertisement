package com.wavelabs;

import javax.servlet.Filter;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.web.DispatcherServletAutoConfiguration;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.DispatcherServlet;

import com.wavelabs.persist.Persister;
import com.wavelabs.record.existence.ObjectChecker;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@EntityScan("com.wavelabs.model")
@EnableJpaRepositories("com.wavelabs.repo")
@ComponentScan({ "com.wavelabs.resource", "com.wavelabs.service", "com.wavelabs.repository",
		"com.wavelabs.solr.service" })
public class JpatestApplication implements CommandLineRunner {

	@Value("${database.url}")
	private static String url;
	
	@Autowired
	private DataSource dataSource;

	@Override
	public void run(String... args) throws Exception {
		org.apache.tomcat.jdbc.pool.DataSource tomcat = (org.apache.tomcat.jdbc.pool.DataSource) dataSource;
		System.err.println(tomcat.getDriverClassName() + ", " + tomcat.getUrl());
	}
	
	public static void main(String[] args) {
		/*SpringApplication application = new SpringApplication(JpatestApplication.class);
		Properties properties = new Properties();
		properties.put("spring.datasource.url", System.getenv(url));
		application.setDefaultProperties(properties);*/
		SpringApplication.run(JpatestApplication.class, args);
	}

	@Bean
	public Filter nbosFilter() {
		return new NbosFilter();
	}

	@Bean
	public AuthenticationTokenCache authenticationTokenCache() {
		return new AuthenticationTokenCache();
	}

	@Bean
	public ObjectChecker objectChecker() {
		return new ObjectChecker();
	}

	@Bean
	public Persister persister() {
		return new Persister();
	}

	@Bean
	public ServletRegistrationBean dispatcherServletRegistration() {
		ServletRegistrationBean registration = new ServletRegistrationBean(dispatcherServlet(), "/*");
		registration.setName(DispatcherServletAutoConfiguration.DEFAULT_DISPATCHER_SERVLET_REGISTRATION_BEAN_NAME);
		return registration;
	}

	/**
	 * To add your own servlet and map it to the root resource. Default would be
	 * root of your application(/)
	 * 
	 * @return DispatcherServlet
	 */
	@Bean
	public DispatcherServlet dispatcherServlet() {
		return new DispatcherServlet();
	}
}
