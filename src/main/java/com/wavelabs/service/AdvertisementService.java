package com.wavelabs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelabs.model.Advertisement;
import com.wavelabs.repository.AdvertisementRepository;

@Service
public class AdvertisementService {

	@Autowired
	private AdvertisementRepository addDao;

	public AdvertisementService(AdvertisementRepository addDao) {

		this.addDao = addDao;
	}

	public boolean persistAdvertisement(Advertisement add) {

		return addDao.persistAdvertisement(add);
	}

	public Advertisement getAdvertisement(int id) {

		return addDao.getAdvertisement(id);
	}

	public Advertisement updateAdvertisement(Advertisement add) {

		return addDao.updateAdvertisement(add);
	}

	public boolean deleteAdvertisement(int id) {
		return addDao.deleteAdvertisement(id);
	}

	public List<Advertisement> getAllAdvertisementOfUser(int id) {

		return addDao.getAllAdvertisementsOfUser(id);
	}

}
