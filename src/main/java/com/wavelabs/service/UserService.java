package com.wavelabs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelabs.model.User;
import com.wavelabs.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userDao;

	public boolean createUser(User user) {
		return userDao.createUser(user);
	}
	
	
	

}
